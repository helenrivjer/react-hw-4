import React, { Component } from "react";
import Card from "../card";
import './app.css'

class App extends Component {
    state = {
       data: []
    }
    componentDidMount() {
        const ajax = new XMLHttpRequest();
        ajax.open('Get', 'https://fakestoreapi.com/products')
        ajax.send()
        ajax.addEventListener('readystatechange', () => {
        if (ajax.readyState === 4 && ajax.status >= 200 && ajax.status < 300) {
           this.setState({data: JSON.parse(ajax.response)}) 
        }else if (ajax.readyState === 4) {
            throw new Error(`Помилка у запиті на сервер : ${ajax.status} / ${ajax.statusText}`)
        }}
        )

    }
    
    render () {
        console.log(this.state)
        const {data} = this.state
        return (
            <>
            <main>
            {data.map((e)=>{
                return <Card product = {e} key = {Math.random()}></Card>      
            })}
            </main>
            </>
        )
    }
}

export default App