import "./card.css"

export default function Card ({product}){
const {title, rating, image, category, description, price} = product

        return(
            <>
            <div className="card">
                    <div className="top">
                        <div className="title">{title}</div>
                        <div className="rating">{(rating.rate/5*100).toFixed(0)}%</div>
                    </div>
                    <div className="item">
                        <img src ={image} alt = {category}></img> 
                        <div className="description">{description}</div>
                    </div>
                    <p>В наявності {rating.count} од.</p>
                    <span>{(price*37*1.2).toFixed(0)} грн</span>
                    <div className="price">{(price*37).toFixed(0)} грн</div>
                
                <button type="submit">Купити</button>
            </div>
            </>
)}
                
          
            